# Fichier de configuration WPA_supplicant pour Eduroam 

### Introduction

Ce fichier de configuration permet de se connecter aux AP "eduroam", en utilisant wpa_supplicant. Pour une raison qui m'échappe totalement, ces AP refusent de fonctionner avec wicd. Et comme les personnes ayant fait la documentation pour mon univ en décidé que GNU/Linux se résume à Ubuntu avec Network-Manager, et n'ont par conséquent pas jugé utile pas publier tous les paramètres nécessaires pour configurer l'authentification manuellement, j'ai décidé de chercher la configuration qui va bien pour la publier.

Afin d'obtenir un fichier de configuration wpa_supplicant fonctionnel, j'ai dû creuser pour trouver les bons paramètres, en me basant sur la doc Network-Manager de ladite univ, mais aussi et surtout sur les informations collectées avec **iwlist**, ainsi qu'un appareil Android déjà connecté sur le réseau Eduraom (la doc est incomplète, car Network-Manager détecte automatiquement certain paramètre).

#### Notes importantes

Ce fichier de configuration ne se prétends donc pas "officiel", ni universel, car les protocoles d'authentification et de chiffrement utilisés peuvent varier d'une univ à l'autre et la plupart des universités ne donne que très peu d'information sur sa configuration, et que ce fichier n'a été testé que sur les AP Eduroam de l'IUT Charlemagne (Université Lorraine). Si le fichier de configuration tel qu'il est ne fonctionne pas pour vous, vous pouvez adapter les paramètres à votre cas.
 
-   en lisant la documentation de votre université
-   en collectant des informations sur les protocoles/paramètres cryptographiques avec *iwlist wlan0 scan*, ainsi que depuis des appareils déjà connectés qui détectent ces informatiques automatiquement et les affichent (en partie)


### Précisions sur la syntaxe

Les paramètres <whatever.tld> et <YourPassword42> sont à remplacer par les paramètres suivants

````
- <login@whatever.tld> : Nom de domaine de l'ENT de l'univ – par exemple "anonymous@univ-lorraine.fr" et "login@[etu.]univ-lorraine.fr" pour le cas de l'UL
    - etu.univ-lorraine.fr : pour les étudiant⋅e⋅s
    - univ-lorraine.fr : pour le personnel
- <YourPassword42> : Mot de passe de l'ENT, le même permettant de se connecter avec "login" sur ent.univ-lorraine.fr
````

### Utilisation de wpa_supplicant

Mettre le fichier de configuration dans /etc sous le nom de wpa_eduroam.conf (ou ailleurs en faisant attention à adapter la commande suivante en conséquence) avant de lancer wpa_supplicant

````
root@linuxbox:~/# wpa_supplicant -B -iwlan0 -c /etc/wpa_eduroam.conf
root@linuxbox:~/# dhclient wlan0
````
